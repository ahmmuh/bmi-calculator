package se.makesite.bmicalculator.screen.splash

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import se.makesite.bmicalculator.screen.calculator.CalculatorActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        CalculatorActivity.start(this, 1)
        finish()
    }
}
