package se.makesite.bmicalculator.screen.calculator

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.calculator_activity.*
import se.makesite.bmicalculator.R

class CalculatorActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val button = findViewById<View?>(R.id.calculate_button) as Button
            button.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View ) {
                println("you killed me !!")
            }

        })

    }
    companion object {
        fun start(context: Context, id: Int){
            val intent = Intent(context, CalculatorActivity::class.java)
            context.startActivity(intent)

        }
    }
}
